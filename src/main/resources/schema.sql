create table building_data (
    data_id integer not null AUTO_INCREMENT,
    building_user varchar(10),
    job_name varchar(50),
    build_number int not null,
    date_time varchar(15) not null,
    console_log varchar(5000),
    build_variables varchar(500),
    primary key(data_id)
);