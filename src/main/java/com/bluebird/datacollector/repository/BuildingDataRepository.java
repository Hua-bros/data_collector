package com.bluebird.datacollector.repository;


import com.bluebird.datacollector.model.BuildingData;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BuildingDataRepository {

    @Insert("Insert into building_data(building_user,job_name,build_number,date_time,console_log,build_variables)"
            + "VALUES(#{currentUser},#{jobName},#{buildNumber},#{dateTime},#{consoleLog},#{buildVariables})")
    int insert(BuildingData data);

}
