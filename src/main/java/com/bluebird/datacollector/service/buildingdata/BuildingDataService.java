package com.bluebird.datacollector.service.buildingdata;

import com.bluebird.datacollector.web.dto.BuildingDataReq;


public interface BuildingDataService {
    int save(BuildingDataReq req);
}
