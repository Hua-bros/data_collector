package com.bluebird.datacollector.service.buildingdata;


import com.bluebird.datacollector.model.BuildingData;
import com.bluebird.datacollector.repository.BuildingDataRepository;
import com.bluebird.datacollector.web.dto.BuildingDataReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.stream.Collectors;

@Service
public class BuildingDataServiceImpl implements BuildingDataService {

    @Autowired
    BuildingDataRepository buildingDataDao;

    @Override
    public int save(BuildingDataReq req) {
        BuildingData data = new BuildingData();
        data.setBuildNumber(req.getBuildNumber());
        data.setJobName(req.getJobName());
        data.setConsoleLog(req.getConsoleLog());
        data.setCurrentUser(req.getCurrentUser());
        data.setDateTime(req.getOpTime());
        data.setBuildVariables(formatMapData(req.getBuildVariables()));
        return buildingDataDao.insert(data);
    }

    private String formatMapData(Map<String, String> map) {
        return (map == null || map.isEmpty())
                ? ""
                : map.entrySet()
                .stream()
                .map(entry -> entry.getKey() + "=" + entry.getValue())
                .collect(Collectors.joining(","));
    }

}
