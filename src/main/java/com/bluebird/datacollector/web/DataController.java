package com.bluebird.datacollector.web;


import com.bluebird.datacollector.service.buildingdata.BuildingDataService;
import com.bluebird.datacollector.web.dto.BuildingDataReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DataController {

    @Autowired
    BuildingDataService buildingDataService;

    @PostMapping(value = "/build_data")
    public RestResult<?> saveBuildingData(@RequestBody BuildingDataReq req){
        buildingDataService.save(req);
        return new RestResult<>().ok();
    }

    @RequestMapping(value = "/health_check")
    public RestResult<?> checkHealth(){
        return new RestResult<>().ok();
    }

}
