package com.bluebird.datacollector.web;

public class RestResult<T> {
    private int code;
    private String message;
    private T payload;

    public RestResult<T> ok(T data){
        this.code = 0;
        this.message = "success";
        this.payload = data;
        return this;
    }

    public RestResult<T> ok(){
        this.code = 0;
        this.message = "success";
        this.payload = null;
        return this;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }
}
