package com.bluebird.datacollector.model;

import java.io.Serializable;

public class BuildingData implements Serializable {

    private static final long serialVersionUID = 18234234234L;

    private String currentUser;
    private String jobName;
    private String buildNumber;
    private String dateTime;
    private String consoleLog;
    private String buildVariables;

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getConsoleLog() {
        return consoleLog;
    }

    public String getBuildVariables() {
        return buildVariables;
    }

    public void setBuildVariables(String buildVariables) {
        this.buildVariables = buildVariables;
    }

    public void setConsoleLog(String consoleLog) {
        this.consoleLog = consoleLog;
    }
}
