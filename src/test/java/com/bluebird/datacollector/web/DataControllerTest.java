package com.bluebird.datacollector.web;

import com.bluebird.datacollector.service.buildingdata.BuildingDataService;
import com.bluebird.datacollector.web.dto.BuildingDataReq;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@RunWith(PowerMockRunner.class)
class DataControllerTest {

    @InjectMocks
    DataController controller;


    @Mock
    BuildingDataService service;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void saveBuildingData() {
        BuildingDataReq data = new BuildingDataReq();
        data.setBuildNumber(System.currentTimeMillis() / 1000 + "");
        data.setJobName("job-" + data.getBuildNumber());
        data.setBuildVariables(new HashMap<>());
        data.setOpTime("2020-12-04");
        data.setCurrentUser("anonymous");
        data.setConsoleLog("3405krfgweprtoweortowpertpoopwertpoksdklfdg0");

        when(service.save(any(BuildingDataReq.class))).thenReturn(1);
        final RestResult<?> restResult = controller.saveBuildingData(data);
        assertTrue(restResult.getCode() == 0);
    }

    @Test
    void checkHealth() {
        final RestResult<?> restResult = controller.checkHealth();
        assertTrue(restResult.getCode() == 0);
    }
}