package com.bluebird.datacollector.service.buildingdata;

import com.bluebird.datacollector.model.BuildingData;
import com.bluebird.datacollector.repository.BuildingDataRepository;
import com.bluebird.datacollector.web.dto.BuildingDataReq;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.HashMap;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@RunWith(PowerMockRunner.class)
class BuildingDataServiceImplTest {

    @Mock
    BuildingDataRepository repository;

    @InjectMocks
    BuildingDataServiceImpl buildingDataService;


    @BeforeEach
    public void setUp() {
        // this will make mocked objects inject into InjectMocked object.
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void save() throws Exception {
        BuildingDataReq data = new BuildingDataReq();
        data.setBuildNumber(System.currentTimeMillis() / 1000 + "");
        data.setJobName("job-" + data.getBuildNumber());
        data.setBuildVariables(new HashMap<>());
        data.setOpTime("2020-12-04");
        data.setCurrentUser("anonymous");
        data.setConsoleLog("3405krfgweprtoweortowpertpoopwertpoksdklfdg0");

        when(repository.insert(any(BuildingData.class))).thenReturn(1);

        final int insert = buildingDataService.save(data);
        assertTrue(insert == 1);
    }


}