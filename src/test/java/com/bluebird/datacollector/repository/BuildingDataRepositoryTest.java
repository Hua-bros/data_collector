package com.bluebird.datacollector.repository;

import com.bluebird.datacollector.model.BuildingData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@RunWith(PowerMockRunner.class)
class BuildingDataRepositoryTest {

    @Mock
    BuildingDataRepository dataRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void insert() {
        BuildingData req = new BuildingData();
        req.setBuildNumber(System.currentTimeMillis() / 1000 + "");
        req.setJobName("job-" + req.getBuildNumber());
        req.setBuildVariables("a=b;c=d;e=f");
        req.setDateTime("2020-12-04");
        req.setCurrentUser("anonymous");
        req.setConsoleLog("3405krfgweprtoweortowpertpoopwertpoksdklfdg0");

        when(dataRepository.insert(any(BuildingData.class))).thenReturn(1);

        assertTrue(dataRepository.insert(req) == 1);
    }
}